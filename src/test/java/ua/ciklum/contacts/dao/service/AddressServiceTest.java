package ua.ciklum.contacts.dao.service;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.ciklum.contacts.Configuration;
import ua.ciklum.contacts.dao.DAO;
import ua.ciklum.contacts.dao.model.Address;
import ua.ciklum.contacts.dao.model.Phone;
import ua.ciklum.contacts.service.address.AddressService;
import ua.ciklum.contacts.service.phone.PhoneService;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = Configuration.class)
public class AddressServiceTest {

    @Autowired
    private AddressService addressService;
    @Autowired
    private PhoneService phoneService;

    @Qualifier("DAOImpl")
    @Autowired
    private DAO dao;

    private List<String> tables = Arrays.asList(Phone.class.getSimpleName(), Address.class.getSimpleName());

    @Before
    public void clearTables(){
        Session sesssion = dao.getSession();
        try {
            for (String table: tables){
                String hql = "delete from "+table;
                sesssion.createQuery(hql).executeUpdate();
            }
        } catch (HibernateException e){
            e.printStackTrace();
        }
        sesssion.close();

    }
    @Test
    public void addAddressTest(){
        addressService.addAddress(new Address("address test, 123"));
    }

    @Test
    public void addAndGetAddressTest(){
        addressService.addAddress(new Address("address test, 123"));
        Assert.assertNotNull(addressService.getAddress("address test, 123"));
    }

    @Test
    public void getAndDelAddressesTest(){
        Phone firstPhone = new Phone("+3801111111111");
        Phone secondPhone = new Phone("+3802222222222");
        phoneService.addPhone(firstPhone);
        phoneService.addPhone(secondPhone);
        Address firstAddr = new Address("first address");
        firstPhone.setAddress(firstAddr);
        secondPhone.setAddress(firstAddr);
        firstAddr.getPhones().add(firstPhone);
        firstAddr.getPhones().add(secondPhone);
        addressService.addAddress(firstAddr);

        Address address = addressService.getAddresses().get(0);
        Assert.assertNotNull(address);
        Assert.assertEquals(firstAddr, address);
        Assert.assertNotNull(address.getPhones());
        Assert.assertEquals(2, address.getPhones().size());

        addressService.delAddress(address);
        Assert.assertEquals(1, addressService.getAddresses().size());
        address.getPhones().clear();
        addressService.delAddress(address);
        Assert.assertEquals(0, phoneService.getPhones().size());
        Assert.assertEquals(0, addressService.getAddresses().size());
    }
}
