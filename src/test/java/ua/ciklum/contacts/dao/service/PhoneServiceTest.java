package ua.ciklum.contacts.dao.service;

import junit.framework.Assert;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.ciklum.contacts.Configuration;
import ua.ciklum.contacts.dao.DAO;
import ua.ciklum.contacts.dao.model.Address;
import ua.ciklum.contacts.dao.model.Phone;
import ua.ciklum.contacts.service.phone.PhoneService;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = Configuration.class)
public class PhoneServiceTest {

    @Autowired
    private PhoneService phoneService;

    @Qualifier("DAOImpl")
    @Autowired
    private DAO dao;
    private List<String> tables = Arrays.asList(Phone.class.getSimpleName(), Address.class.getSimpleName());

    @Before
    public void clearTables(){
        Session sesssion = dao.getSession();
        try {
            for (String table: tables){
                String hql = "delete from "+table;
                sesssion.createQuery(hql).executeUpdate();
            }
        } catch (HibernateException e){
            e.printStackTrace();
        }
        sesssion.close();

    }

    @Test
    public void addPhoneTest(){
        Phone phone = new Phone("+3801111111111");
        phoneService.addPhone(phone);
    }

    @Test
    public void addandGetPhoneTest(){
        Phone phone = new Phone("+3801111111111");
        phoneService.addPhone(phone);
        List<Phone> phones = phoneService.getPhones();
        Assert.assertTrue(phones.contains(phone));
    }

    @Test
    public void getPhonesTest(){
        phoneService.addPhone(new Phone("+380954411999"));
        Phone phone = phoneService.getPhones().get(0);
        Assert.assertNotNull(phone);
    }

    @Test
    public void delPhoneTest(){
        phoneService.addPhone(new Phone("+380954411999"));
        Phone phone = phoneService.getPhones().get(0);
        Assert.assertNotNull(phone);
        phoneService.delPhone(phone);
        Assert.assertEquals(0, phoneService.getPhones().size());
    }

}
