package ua.ciklum.contacts.service.address;

import org.springframework.stereotype.Service;
import ua.ciklum.contacts.dao.model.Address;

import java.util.List;

@Service
public interface AddressService {

    void addAddress(Address address);
    void delAddress(Address address);
    void updateAddress(Address address);
    Address getAddress(String address);
    List<Address> getAddresses();
}
