package ua.ciklum.contacts.service.address;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ciklum.contacts.dao.DAO;
import ua.ciklum.contacts.dao.model.Address;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private DAO dao;

    @Override
    public void addAddress(Address address) {
        dao.getSession().save(address);
    }

    @Override
    public void delAddress(Address address) {
        dao.getSession().flush();
        dao.getSession().delete(address);
    }

    @Override
    public void updateAddress(Address address) {
        dao.getSession().update(address);
    }

    @Override
    public Address getAddress(String address) {
        return (Address) dao.getSession().createCriteria(Address.class).add(Restrictions.eq("address", address)).uniqueResult();
    }

    @Override
    public List<Address> getAddresses() {
        return dao.getSession().createCriteria(Address.class).list();
    }
}
