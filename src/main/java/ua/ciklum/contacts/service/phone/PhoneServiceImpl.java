package ua.ciklum.contacts.service.phone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ciklum.contacts.dao.DAO;
import ua.ciklum.contacts.dao.model.Address;
import ua.ciklum.contacts.dao.model.Phone;

import java.util.List;

@Service
public class PhoneServiceImpl implements PhoneService {

    @Autowired
    private DAO dao;

    @Override
    public void addPhone(Phone phone) {
        dao.getSession().save(phone);
    }

    @Override
    public void delPhone(Phone phone) {
        dao.getSession().flush();
        dao.getSession().beginTransaction();
        dao.getSession().delete(phone);
        dao.getSession().getTransaction().commit();
    }

    @Override
    public List<Phone> getPhones() {
        return dao.getSession().createCriteria(Phone.class).list();
    }

    @Override
    public List<Phone> getPhonesByAddress(Address address) {
        return dao.getSession().createQuery("from Phone where address=:address")
                .setParameter("address", address.getAddress()).list();
    }
}
