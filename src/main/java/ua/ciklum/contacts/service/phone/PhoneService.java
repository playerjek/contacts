package ua.ciklum.contacts.service.phone;

import org.springframework.stereotype.Service;
import ua.ciklum.contacts.dao.model.Address;
import ua.ciklum.contacts.dao.model.Phone;

import java.util.List;

@Service
public interface PhoneService {

    void addPhone(Phone phone);
    void delPhone(Phone phone);
    List<Phone> getPhones();
    List<Phone> getPhonesByAddress(Address address);
}
