package ua.ciklum.contacts.dao.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "address", unique = true)
    private String address;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "address", orphanRemoval=true, cascade=CascadeType.ALL)
    private Set<Phone> phones = new HashSet<>();

    public Address(String address){
        this.address = address;
    }

    public Address() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    @Override
    public String toString(){
        return "address: "+address+", phones: "+phones;
    }
}
