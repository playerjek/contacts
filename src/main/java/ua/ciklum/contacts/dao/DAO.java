package ua.ciklum.contacts.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

@Component
public interface DAO {

    Session getSession();
}
