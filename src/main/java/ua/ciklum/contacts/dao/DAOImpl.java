package ua.ciklum.contacts.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

@Component
public class DAOImpl implements DAO{

    @Override
    public Session getSession() {
        return HibernateUtil.getSession();
    }
}
