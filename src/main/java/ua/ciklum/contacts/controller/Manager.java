package ua.ciklum.contacts.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.ciklum.contacts.controller.requests.PhoneAddr;
import ua.ciklum.contacts.dao.model.Address;
import ua.ciklum.contacts.dao.model.Phone;
import ua.ciklum.contacts.service.address.AddressService;
import ua.ciklum.contacts.service.phone.PhoneService;

import java.util.List;

@Controller("/")
public class Manager {

    @Autowired
    private AddressService addressService;
    @Autowired
    private PhoneService phoneService;
    private static final Logger log = LoggerFactory.getLogger(Manager.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView present(){
        try {
            ModelAndView model = new ModelAndView("contacts");
            List<Address> addresses = addressService.getAddresses();
            log.info("List of addresses: {}", addresses);
            model.addObject("addresses", addresses);
            return model;
        } catch (Exception e){
            log.error("", e);
            return new ModelAndView("contacts");
        }
    }

    @RequestMapping(value = "/addPhone", method = RequestMethod.POST)
    public @ResponseBody String addPhone(@RequestBody PhoneAddr phoneaddress){
        try {
            Address addr = addressService.getAddress(phoneaddress.getAddress());
            if (phoneaddress.getPhone() != null && phoneaddress.getPhone().length()>0) {
                addr.getPhones().add(new Phone(phoneaddress.getPhone()));
                addressService.updateAddress(addr);
                return "success";
            } else {
                return "phone not added";
            }
        } catch (Exception e){
            log.error("", e);
            return "failed add phone";
        }
    }

    @RequestMapping(value = "/removePhone", method = RequestMethod.POST)
    public @ResponseBody String removePhone(@RequestBody PhoneAddr phoneaddress){
        try {
            if (phoneaddress == null) return "phone not present";
            log.info("Request remove phone: {}, address: {}", phoneaddress.getPhone(), phoneaddress.getAddress());
            Address addr = addressService.getAddress(phoneaddress.getAddress());
            Phone phToRemove = null;
            for (Phone ph: addr.getPhones()) {
                if (ph.getPhone().equals(phoneaddress.getPhone())) {
                    phToRemove = ph;
                }
            }
            if (phToRemove != null) {
                addr.getPhones().remove(phToRemove);
                addressService.updateAddress(addr);
                return "success";
            } else {
                return "phone not exist in this address";
            }
        } catch (Exception e){
            log.error("", e);
            return "fail";
        }
    }

    @RequestMapping(value = "/addAddress", method = RequestMethod.POST)
    public @ResponseBody String addAddress(@RequestBody String address){
        try {
            log.info("Request add address: {}", address);
            addressService.addAddress(new Address(address));
            return "success";
        } catch (Exception e) {
            log.error("", e);
            return "Error during saving data";
        }
    }

    @RequestMapping(value = "/removeAddress", method = RequestMethod.GET)
    public @ResponseBody String removeAddress(@RequestBody Address address){
        try {
            log.info("request remove address: {}", address!=null? address.getAddress(): address);
            addressService.delAddress(address);
            return "success";
        } catch (Exception e){
            log.error("", e);
            return "Error during remove phone";
        }
    }
}
