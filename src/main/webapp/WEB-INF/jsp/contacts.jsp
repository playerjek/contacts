<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Contacts manager</title>
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    </head>
    <body>
        <h1>Welcome contacts manager!</h1>
        <table id="addresses">
            <caption>Contacts</caption>
            <tr>
                <%--<th>ID</th>--%>
                <th>Address</th>
                <th>Phones</th>
            </tr>
            <c:forEach items="${addresses}" var="addr">
                <tr>
                    <%--<td>${addr.id}</td>--%>
                    <td id="addr_${addr.id}">${addr.address}</td>
                    <td id="phones_${addr.id}">${addr.phones}</td>
                </tr>
            </c:forEach>
        </table>
        <br><br>
        <h3>To add a new address, enter the address and click the "Add Address".</h3>
        <input id="add_address_input" type="text" width="50px">
        <button id="add_address_button">add address</button>
        <br><br>
        <h3>Choose address, enter the phone and click "add phone", to add a phone to the address.</h3>
        <select id="add_phone_addr_select">
            <option disabled>choose address</option>
            <c:forEach items="${addresses}" var="addr">
                <option>${addr.address}</option>
            </c:forEach>
        </select>
        <input id="add_phone_input" type="text" width="50px">
        <button id="add_phone_button">add phone</button>
        <br><br>
        <h3>Choose address, enter the phone and click "remove phone", to remove a phone from the address.</h3>
        <select id="remove_phone_addr_select" onchange=showSelector(this)>
            <option disabled>choose address</option>
            <c:forEach items="${addresses}" var="addr">
                <option id="addr_opt ${addr.id}">${addr.address}</option>
            </c:forEach>
        </select>
        <input id="remove_phone_input" type="text" width="50px">
        <button id="remove_phone_button">remove phone</button>

        <script type='text/javascript'>
            $("#add_address_button").click(function(){
                var address = document.getElementById("add_address_input").value;
                if(address.length > 0){
                    $.ajax({
                        type: "post",
                        url: "${pageContext.request.contextPath}/addAddress",
                        data: address,
                        contentType: "text/plain",
                        success: function(textStatus) {
                            alert(textStatus);
                            document.getElementById("add_address_input").value='';
                            window.location.reload();
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            alert(textStatus);
                            console.log(errorThrown);
                        }
                    })
                } else {
                    alert("First enter the address");
                }
            });

            $("#add_phone_button").click(function(){
                var select = document.getElementById("add_phone_addr_select");
                var address = select.options[select.selectedIndex].value;
                var phone = document.getElementById("add_phone_input").value;
                var RE = /^((\+?\d+)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
                if (phone.length > 0 && RE.test(phone)) {
                    $.ajax({
                        type: "post",
                        url: "${pageContext.request.contextPath}/addPhone",
                        data: "{\"address\":\"" + address + "\",\"phone\":\"" + phone + "\"}",
                        contentType: "application/json",
                        success: function (textStatus) {
                            alert(textStatus);
                            document.getElementById("add_phone_input").value = '';
                            window.location.reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert(textStatus);
                            console.log(errorThrown);
                        }
                    })
                } else {
                    alert("Please, enter correct phone");
                }
            });

            $("#remove_phone_button").click(function() {
                var select = document.getElementById("remove_phone_addr_select");
                var address = select.options[select.selectedIndex].value;
                var phone = document.getElementById("remove_phone_input").value;
                var tableAddrrows = document.getElementById("addresses").rows;
                var isPhoneFromCorrectAddress = false;
                for (var i=0; i<tableAddrrows.length; i++){
                    var phones = tableAddrrows[i].cells[1].firstChild.data;
                    var addr = tableAddrrows[i].cells[0].firstChild.data;
                    if(address.localeCompare(addr)==0 && phones.indexOf(phone) > -1){
                        isPhoneFromCorrectAddress = true;
                        break;
                    }
                }
                var RE = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
                if (phone.length > 0 && RE.test(phone) && isPhoneFromCorrectAddress) {
                    $.ajax({
                        type: "post",
                        url: "${pageContext.request.contextPath}/removePhone",
                        data: "{\"address\":\"" + address + "\",\"phone\":\"" + phone + "\"}",
                        contentType: "application/json",
                        success: function (textStatus) {
                            alert(textStatus);
                            document.getElementById("remove_phone_input").value = '';
                            window.location.reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert(textStatus);
                            console.log(errorThrown);
                        }
                    })
                } else {
                    alert("Please, enter correct phone from selected address");
                }
            });
        </script>
    </body>
</html>
