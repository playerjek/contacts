Hello, contacts-manager is web application. 
First, to run this app you need tomcat server. 
How to run? use two commands:
mvn clean install
if standart port free:
mvn tomcat7:run
else
mvn -Dmaven.tomcat.port=port tomcat7:run
where port - is free port number.
Then app will be available at the address:
http://localhost:port/contacts-manager/

How to use?
At main page you will see empty table with two columns: addresses and associated with them phone numbers. You can add new address and then new phone number for him. Also you can remove phone number from association with address. For switching between existing addresses please use the drop down list. Unfortunately the rest of the action, I did not finished on time.

Framworks that I used is:
Hibernate - for work with database, if database has changed to another we must only change dialect, not any more;
Spring ioc - for dependency injection, Spring initialized beans (or Objects) instead of us;
Spring mvc - for implementation MVC pattern (client-server interaction);
JUnit - for tests;
H2 in memory - database for this app;
Maven - used to automatically build the project and run tests.

Also I used compiler, war and tomcat7-maven plugins.